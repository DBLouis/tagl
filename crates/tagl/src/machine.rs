use std::io;
use winreg::{enums::*, RegKey};

const HKLM: RegKey = RegKey::predef(HKEY_LOCAL_MACHINE);

#[derive(Debug)]
pub struct Machine {
    pub guid: Box<str>,
    pub name: Box<str>,
    pub version: u32,
}

impl Machine {
    pub fn new() -> io::Result<Self> {
        let key = HKLM.open_subkey("SOFTWARE\\Microsoft\\Cryptography")?;
        let guid: String = key.get_value("MachineGuid")?;
        let guid = guid.into_boxed_str();

        let key = HKLM.open_subkey("SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ActiveComputerName")?;
        let name: String = key.get_value("ComputerName")?;
        let name = name.into_boxed_str();

        let key = HKLM.open_subkey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion")?;
        let version: u32 = key.get_value("CurrentMajorVersionNumber").unwrap_or(0);

        Ok(Self { guid, name, version })
    }
}
