#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]
#![cfg(windows)]

//! # The Amazing Game Launcher
//!
//! A package manager for games

pub mod crash;
pub mod logger;
pub mod machine;

use crate::machine::Machine;
use anyhow::{anyhow, bail, ensure, Context};
use crossbeam_channel::{Receiver, Sender};
use directories::ProjectDirs;
use gooey::{
    view,
    view::{Action, Entry, Event},
};
use log::{debug, error, info, warn};
use std::{
    any::Any,
    fs,
    fs::File,
    io, panic,
    path::{Path, PathBuf},
    str::FromStr,
    thread,
    time::{Duration, Instant},
};
use time::{macros::format_description, OffsetDateTime};
use tpm::{
    client::Client, config::Config, inventory::Inventory, manager::Manager, manifest::Manifest, repository::Repository,
};
use windows::mutex::GlobalMutex;
use wine::Wine;

const UUID: &str = "d1794c96-8302-47c9-b0ea-79f936cdd0a8";

fn path_to_repository_file(home: &ProjectDirs) -> PathBuf {
    home.config_dir().join("repository.yml")
}

fn path_to_config_file(home: &ProjectDirs) -> PathBuf {
    home.config_dir().join("config.yml")
}

fn path_to_inventory_file(home: &ProjectDirs) -> PathBuf {
    home.data_dir().join("inventory.yml")
}

pub fn launch() -> anyhow::Result<()> {
    // Ensure single instance
    let _guard = GlobalMutex::lock(UUID).context("launcher is already running")?;

    let home = ProjectDirs::from("", "", env!("CARGO_PKG_NAME")).ok_or(anyhow!("home directory not found"))?;

    // Ensure needed directories exist
    fs::create_dir_all(home.data_dir())?;
    fs::create_dir_all(home.config_dir())?;
    fs::create_dir_all(home.cache_dir())?;

    let format = format_description!("[day]-[month]-[year]-[hour].[minute].[second]");
    let timestamp = OffsetDateTime::now_local()
        .unwrap_or_else(|_| OffsetDateTime::now_utc())
        .format(&format)
        .unwrap();

    let mut path = home.data_dir().join("instances");
    let _ = fs::create_dir(&path);
    path.push(&timestamp);

    fs::create_dir(&path).context("failed to create instance directory")?;

    // Initialize logger
    logger::init(&path.join("log.txt"))?;

    // Initialize crash handler
    crash::init(&path.join("backtrace.txt"));

    // Fetch machine informations
    let machine = Machine::new().context("failed to get machine information")?;
    if let Some(wine) = Wine::try_load() {
        info!("Wine v{} - build {}", wine.version(), wine.build_id());
        let (sysname, release) = wine.host_version();
        info!("Host {} - {}", sysname, release);
    } else {
        /* Not running in Wine */
        ensure!(machine.version >= 10, "windows {} is not supported", machine.version);
    }
    info!("{:?}", machine);

    let (actions_, actions) = crossbeam_channel::bounded(8);
    let (events, events_) = crossbeam_channel::bounded(8);

    let mut manager = if let Some(manager) = try_load(&home)? {
        manager
    } else {
        let path = home.data_dir().join("packages");
        let (repository, config) = gooey::setup::show(&path).ok_or(anyhow!("cancelled"))?;
        init(&home, repository, config)?
    };

    let handle = thread::spawn(move || run(&mut manager, actions, events));

    if let Err(e) = panic::catch_unwind(move || {
        view::show(actions_, events_);
    }) {
        error!("Main thread panicked: {:?}", get_panic_message(e));
        bail!("Main thread panicked!")
    }

    if let Err(e) = handle.join() {
        error!("Event loop panicked: {:?}", get_panic_message(e));
        bail!("Event loop panicked!")
    }

    Ok(())
}

fn get_panic_message(panic: Box<dyn Any>) -> Option<String> {
    panic
        .downcast::<String>()
        .map(|b| *b)
        .or_else(|panic| panic.downcast::<&'static str>().map(|s| (*s).to_owned()))
        .ok()
}

fn try_load(home: &ProjectDirs) -> anyhow::Result<Option<Manager>> {
    let client = {
        let Ok(file) = File::open(path_to_repository_file(home)) else {
            // Repository file does not exist
            return Ok(None)
        };
        let data = io::read_to_string(file)?;
        let repository = Repository::load(&data).context("failed to load repository")?;
        Client::new(repository)?
    };

    if let Some(config) = Config::try_load(&path_to_config_file(home))? {
        debug!("{:?}", config);
        let path = path_to_inventory_file(home);
        if let Some(inventory) = Inventory::try_load(&path)? {
            debug!("{:?}", inventory);
            Ok(Some(Manager::new(client, config, inventory)))
        } else {
            make_manager(&path, client, config).map(Some)
        }
    } else {
        // Config file does not exist
        Ok(None)
    }
}

fn init(home: &ProjectDirs, repository: Repository, config: Config) -> anyhow::Result<Manager> {
    let _ = fs::create_dir(&config.install_path);

    let file = File::create(path_to_repository_file(home))?;
    repository.save(file)?;
    debug!("{:?}", &repository);

    let client = Client::new(repository)?;

    config.save(&path_to_config_file(home))?;
    debug!("{:?}", config);

    make_manager(&path_to_inventory_file(home), client, config)
}

fn make_manager(path: &Path, mut client: Client, config: Config) -> anyhow::Result<Manager> {
    // Get repository manifest
    let manifest = {
        let data = client.read_to_string("manifest.yml")?;
        Manifest::from_str(&data)?
    };
    debug!("{:?}", manifest);

    // Initialize inventory from manifest
    let inventory = Inventory::new(path, manifest);
    debug!("{:?}", inventory);
    inventory.save()?;

    let manager = Manager::new(client, config, inventory);
    debug!("{:?}", manager);

    Ok(manager)
}

fn make_entries(manager: &mut Manager) -> Box<[Entry]> {
    manager
        .inventory()
        .iter()
        .map(|(package, state)| Entry::new(package.clone(), *state))
        .collect::<Vec<_>>()
        .into_boxed_slice()
}

fn run(manager: &mut Manager, actions: Receiver<Action>, events: Sender<Event>) {
    let mut cancel = false;

    let entries = make_entries(manager);
    view::notify(&events, Event::InventoryInit { entries });

    while let Ok(action) = actions.recv() {
        // Map UI actions to functions
        match action {
            Action::Upgrade => todo!(),
            Action::InventoryUpdate => {
                let result = manager.update_inventory().map(|_| make_entries(manager));
                view::notify(&events, Event::InventoryUpdate { result });
            }
            Action::PackageDownload { uuid } => {
                let mut last = Instant::now();
                let mut bytes_before = 0;

                let title = manager.inventory().package(uuid).title.clone();
                view::notify(&events, Event::PackageDownloadBegin { title });

                let result = manager.download_package(uuid, |ratio, bytes| {
                    let now = Instant::now();
                    let elapsed = now - last;

                    if elapsed >= Duration::from_millis(500) {
                        last = now;
                        let speed = ((bytes - bytes_before) * 1000) / elapsed.as_millis() as u64;
                        bytes_before = bytes;
                        view::notify(&events, Event::PackageDownloadProgress { ratio, speed });
                    }

                    !cancel
                });

                view::notify(&events, Event::PackageDownloadEnd { result });
            }
            Action::PackageDownloadCancel => {
                cancel = true;
            }
            Action::PackageDelete { uuid } => {
                let result = manager.delete_package(uuid);
                view::notify(&events, Event::PackageDeleteEnd { result });
            }
            Action::PackageInstall { uuid } => {
                let result = manager.install_package(uuid);
                view::notify(&events, Event::PackageInstallEnd { result });
            }
            Action::PackageUninstall { uuid } => {
                let result = manager.uninstall_package(uuid);
                view::notify(&events, Event::PackageUninstallEnd { result });
            }
            Action::PackageExecute { uuid } => {
                let result = manager.execute_package(uuid);
                view::notify(&events, Event::PackageExecute { result });
            }
            Action::Exit => {
                debug!("Main loop exit");
                break;
            }
        }
    }
}
