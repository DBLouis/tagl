use log::error;
use std::{backtrace::Backtrace, fs::File, io::Write, panic, path::Path};

pub fn init(path: &Path) {
    let path = path.to_owned();
    panic::set_hook(Box::new(move |info| {
        error!("{}", &info);
        let mut file = File::create(&path).unwrap();
        let backtrace = format!("{:#?}", Backtrace::force_capture());
        file.write_all(backtrace.as_bytes()).unwrap();
    }));
}
