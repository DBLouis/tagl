use std::env;
use winresource::WindowsResource;

fn main() {
    assert_eq!(&env::var("CARGO_CFG_TARGET_OS").unwrap(), "windows");

    let mut res = WindowsResource::new();
    res.set_toolkit_path("/usr/local/bin");
    res.set_manifest_file("manifest.xml");
    res.set_icon("tagl.ico");
    res.compile().unwrap();
}
