use crate::{MAGIC, ZSTD_LEVEL, ZSTD_WINDOW_LOG};
use blake3::Hash;
use buffer_redux::{BufReader, Buffer};
use integer_encoding::VarIntWriter;
use log::{debug, warn};
use std::io::{BufRead, Error, ErrorKind, Result, Write};
use windows::{
    fs::File,
    path::{Path, WalkDir},
};
use zstd::Encoder;

/// Creates a new package archive from a source directory to a target file.
///
/// # Arguments
/// * `id` - The unique identifier of the package.
/// * `version` - The version of the package.
/// * `script` - The package script configuration script.
/// * `source` - The source directory to archive.
/// * `target` - The target file to write the archive to.
pub fn archive(script: impl AsRef<str>, source: impl AsRef<Path>, target: impl AsRef<Path>) -> Result<Hash> {
    let source = source.as_ref();
    let target = target.as_ref();

    if !source.exists() {
        warn!("Source directory does not exist");
        return Err(Error::from(ErrorKind::NotFound));
    }
    if !source.is_dir() {
        warn!("Source is not a directory");
        return Err(Error::from(ErrorKind::InvalidInput));
    }
    if target.exists() {
        warn!("Target file already exists");
        return Err(Error::from(ErrorKind::AlreadyExists));
    }

    let mut buffer = None;

    let mut file = File::create(target).unwrap();
    file.write_all(&MAGIC)?;
    file.flush()?;

    let mut encoder = Encoder::new(file, ZSTD_LEVEL)?;
    encoder.include_checksum(false)?;
    encoder.long_distance_matching(true)?;
    encoder.window_log(ZSTD_WINDOW_LOG)?;

    write_script(script.as_ref(), &mut encoder)?;
    visit(source, source, &mut buffer, &mut encoder)?;

    let file = encoder.finish()?;
    let map = file.map().unwrap();
    let view = map.view().unwrap();
    let hash = blake3::hash(&view);
    Ok(hash)
}

fn write_script<W: Write>(script: &str, writer: &mut W) -> Result<()> {
    let size = u32::try_from(script.len()).map_err(|e| Error::new(ErrorKind::InvalidInput, e))?;
    writer.write_varint(size)?;
    writer.write_all(script.as_bytes())?;
    Ok(())
}

fn write_path<W: Write>(path: &Path, writer: &mut W) -> Result<()> {
    writer.write_all(path.as_bytes_with_nul())?;
    Ok(())
}

fn write_file<R: BufRead, W: Write>(mut reader: R, writer: &mut W, size: u64) -> Result<()> {
    writer.write_varint(size)?;
    // Write file data
    loop {
        let bytes = reader.fill_buf()?;
        if bytes.is_empty() {
            break;
        }
        let n = bytes.len();
        writer.write_all(bytes)?;
        reader.consume(n);
    }
    Ok(())
}

fn add<W: Write>(source: &Path, path: &Path, buffer: &mut Option<Buffer>, writer: &mut W) -> Result<()> {
    let file = File::open(path).unwrap();
    let size = file.len().unwrap() as _;

    // Make path relative to source
    let path = path.strip_prefix(source).expect("should be prefixed by source path");
    debug!("Writing relative file path: {}", path.display());
    write_path(path, writer)?;

    // NOTE Shenaningans to reuse buffer
    let mut reader = if let Some(buffer) = buffer.take() {
        BufReader::with_buffer(buffer, file)
    } else {
        BufReader::new_ringbuf(file)
    };

    debug!("Writing file data of size: {} bytes", size);
    write_file(&mut reader, writer, size)?;

    // NOTE Shenaningans to reuse buffer
    buffer.replace({
        let (_, buffer) = reader.into_inner_with_buffer();
        assert!(buffer.is_empty());
        buffer
    });

    Ok(())
}

fn visit<W: Write>(source: &Path, path: &Path, buffer: &mut Option<Buffer>, writer: &mut W) -> Result<()> {
    if path.is_dir() {
        debug!("Visiting directory: {}", path.display());
        let walk = WalkDir::new(path);
        for entry in walk {
            let entry = entry.unwrap();
            let file_name = &*entry.file_name();
            let path = path.join(file_name);
            visit(source, &path, buffer, writer)?;
        }
    } else {
        debug!("Adding file: {}", path.display());
        add(source, path, buffer, writer)?;
    }
    Ok(())
}
