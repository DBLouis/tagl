#![cfg(all(windows, target_endian = "little"))]
#![forbid(unsafe_code)]
#![warn(missing_debug_implementations)]
//! ZPKG is a package format for archiving and distributing software packages.
//! ZPKG is very simple and does not record files metadata.
//! ZPKG is designed to be as fast as possible for clients to extract packages.

mod archive;
mod extract;

/// The ZPKG magic number.
/// Those are the first 8 bytes of a ZPKG file.
pub const MAGIC: &[u8] = b"ZPKGv1.0";

pub const ZSTD_LEVEL: i32 = 5;
pub const ZSTD_WINDOW_LOG: u32 = 23;

pub use archive::archive;
pub use extract::extract;
