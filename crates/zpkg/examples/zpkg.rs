use log::{debug, error, info};
use std::env;
use windows::path::{Path, PathBuf};

pub fn main() {
    env_logger::init();
    info!("ZPKG v{}", env!("CARGO_PKG_VERSION"));

    let mut args = env::args_os();
    let _ = args.next(); // Skip program name

    let command = args.next().unwrap();
    let id: u32 = args.next().unwrap().to_string_lossy().parse().unwrap();
    let version: u32 = args.next().unwrap().to_string_lossy().parse().unwrap();
    let source = PathBuf::from_os_str(args.next().unwrap());
    let target = PathBuf::from_os_str(args.next().unwrap());

    match command.to_str().unwrap() {
        "archive" => {
            let script = args.next().unwrap().to_string_lossy().to_string();

            let mut target = target;
            let name = format!("{}-{}.zpkg", id, version);
            target.push(PathBuf::from_str(&name).unwrap());

            debug!("Archiving package {id} v{version}");
            debug!("Script: {script}");
            debug!("Source: {}", source.display());
            debug!("Target: {}", target.display());
            archive(&script, &source, &target);
        }
        "extract" => {
            let mut target = target;
            let name = format!("{}-{}", id, version);
            target.push(PathBuf::from_str(&name).unwrap());

            debug!("Extracting package {id} v{version}");
            debug!("Source: {}", source.display());
            debug!("Target: {}", target.display());
            extract(&source, &target);
        }
        unknown => {
            error!("Invalid command '{unknown}'");
        }
    }
}

fn archive(script: &str, source: &Path, target: &Path) {
    match zpkg::archive(script, source, target) {
        Ok(hash) => info!("Package archived successfully: {}", hash),
        Err(e) => error!("Failed to archive package: {}", e),
    }
}

fn extract(source: &Path, target: &Path) {
    match zpkg::extract(source, target) {
        Ok(()) => info!("Package extracted successfully"),
        Err(e) => error!("Failed to extract package: {}", e),
    }
}
