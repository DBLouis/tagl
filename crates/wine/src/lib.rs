//! A helper crate to detect if running in Wine.
//!
//! # Example
//! ```
//! use wine::Wine;
//!
//! if let Some(wine) = Wine::try_load() {
//!     println!("{}", wine.version());
//!     println!("{}", wine.build_id());
//!     let (sysname, release) = wine.host_version();
//!     println!("{} - {}", sysname, release);
//! } else {
//!     println!("Not running in Wine.");
//! }
//! ```
#![cfg(windows)]
#![deny(clippy::undocumented_unsafe_blocks)]
#![deny(missing_debug_implementations)]
#![deny(missing_docs)]
#![deny(unsafe_op_in_unsafe_fn)]

use std::{
    ffi::{c_char, CStr},
    mem,
    mem::MaybeUninit,
};
use windows_sys::s;
use windows_sys::Win32::{
    Foundation::HMODULE,
    System::LibraryLoader::{FreeLibrary, GetModuleHandleExA, GetProcAddress},
};

/// Represents information about Wine if it's detected.
#[derive(Debug)]
pub struct Wine<'a> {
    // NOTE: The 'a lifetime is valid as long as the module is loaded.
    module: HMODULE,
    version: &'a str,
    build_id: &'a str,
    host_version: (&'a str, &'a str),
}

fn get_nt_module_handle() -> Option<HMODULE> {
    let mut out = MaybeUninit::uninit();
    // This will increase the module reference count.
    // https://learn.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getmodulehandleexa
    // SAFETY: Win32 foreign function.
    let ret = unsafe { GetModuleHandleExA(0, s!("ntdll"), out.as_mut_ptr()) };
    if ret == 0 {
        return None;
    }
    // SAFETY: Return value is not zero so it is safe to assume `out` is initialized.
    Some(unsafe { out.assume_init() })
}

macro_rules! get_function {
    ($module:ident, $name:literal, $type:ty) => {{
        // https://learn.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getprocaddress
        // SAFETY: Win32 foreign function.
        let f = unsafe { GetProcAddress($module, s!($name))? };
        // SAFETY: Function signature matches given type.
        unsafe { mem::transmute::<_, $type>(f) }
    }};
}

impl<'a> Wine<'a> {
    /// Attempts to load information about Wine.
    ///
    /// Returns `Some(Wine)` if Wine is detected, `None` otherwise.
    pub fn try_load() -> Option<Self> {
        let module = get_nt_module_handle()?;

        let version = {
            type F = unsafe extern "stdcall" fn() -> *const c_char;
            let f = get_function!(module, "wine_get_version", F);
            // SAFETY: Calling foreign function.
            let s = unsafe { f() };
            // SAFETY: Returned C string pointer assumed to be valid.
            let s = unsafe { CStr::from_ptr(s) };
            s.to_str().ok()?
        };

        let build_id = {
            type F = unsafe extern "stdcall" fn() -> *const c_char;
            let f = get_function!(module, "wine_get_build_id", F);
            // SAFETY: Calling foreign function.
            let s = unsafe { f() };
            // SAFETY: Returned C string pointer assumed to be valid.
            let s = unsafe { CStr::from_ptr(s) };
            s.to_str().ok()?
        };

        let host_version = {
            type F = unsafe extern "stdcall" fn(*mut *const c_char, *mut *const c_char);
            let f = get_function!(module, "wine_get_host_version", F);

            let mut sysname = MaybeUninit::uninit();
            let mut release = MaybeUninit::uninit();
            // SAFETY: Calling foreign function.
            unsafe {
                f(sysname.as_mut_ptr(), release.as_mut_ptr());
            }
            // SAFETY: Returned C string pointer assumed to be valid.
            let sysname = unsafe {
                let ptr = sysname.assume_init();
                CStr::from_ptr(ptr).to_str().ok()?
            };
            // SAFETY: Returned C string pointer assumed to be valid.
            let release = unsafe {
                let ptr = release.assume_init();
                CStr::from_ptr(ptr).to_str().ok()?
            };
            (sysname, release)
        };

        Some(Self {
            module,
            version,
            build_id,
            host_version,
        })
    }

    /// Get the Wine version.
    pub fn version(&self) -> &str {
        self.version
    }

    /// Get the Wine build ID.
    pub fn build_id(&self) -> &str {
        self.build_id
    }

    /// Get the host system's version and release information.
    pub fn host_version(&self) -> (&str, &str) {
        self.host_version
    }
}

impl<'a> Drop for Wine<'a> {
    fn drop(&mut self) {
        // This will decrease the module's reference count and free it as needed.
        // SAFETY: Win32 function called correctly.
        // The module handle was properly acquired by calling `GetModuleHandleExA`.
        let _ = unsafe { FreeLibrary(self.module) };
    }
}
