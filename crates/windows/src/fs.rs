use crate::{error::Error, path::Path};
use std::{mem::MaybeUninit, ptr};

use windows_sys::Win32::Foundation::ERROR_SUCCESS;
use windows_sys::Win32::Storage::FileSystem::{CreateDirectoryW, GetDiskSpaceInformationW};

mod file;
mod walk;

pub use file::{File, FileMap, FileMapMut, FileView, FileViewMut};
pub use walk::WalkDir;

/// Returns the amount of available space in bytes on the volume of the specified path.
pub fn get_available_space(path: &Path) -> Result<u64, Error> {
    let mut info = MaybeUninit::uninit();

    // SAFETY: Win32 foreign function.
    let rc = unsafe {
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getdiskspaceinformationw
        GetDiskSpaceInformationW(path.as_ptr(), info.as_mut_ptr())
    };

    if rc != ERROR_SUCCESS as _ {
        return Err(Error::from_win32());
    }

    // SAFETY: `rc` is ERROR_SUCCESS, therefore `info` is initialized.
    let info = unsafe { info.assume_init() };

    let available = info.CallerAvailableAllocationUnits;
    let sectors = info.SectorsPerAllocationUnit as u64;
    let bytes = info.BytesPerSector as u64;
    Ok(available * sectors * bytes)
}

pub fn create_dir(path: &Path) -> Result<(), Error> {
    // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createdirectoryw
    // SAFETY: Win32 foreign function.
    let rc = unsafe { CreateDirectoryW(path.as_ptr(), ptr::null_mut()) };
    if rc == 0 {
        return Err(Error::from_win32());
    }
    Ok(())
}

pub fn create_dir_all(path: &Path) -> Result<(), Error> {
    if path.is_dir() {
        return Ok(());
    }
    if let Some(parent) = path.parent() {
        create_dir_all(&parent)?;
    }
    create_dir(path)
}
