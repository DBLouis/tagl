use std::{convert::TryFrom, ffi::OsStr, ops};
use widestring::{
    error::{ContainsNul, NulError},
    ucstr::Display,
    U16CStr, U16CString,
};
use windows_sys::Win32::Storage::FileSystem::{GetFileAttributesW, FILE_ATTRIBUTE_DIRECTORY, INVALID_FILE_ATTRIBUTES};

const NUL_TERMINATOR: u16 = 0x0000;
const MAIN_SEPARATOR: u16 = 0x005C;
const DRIVE_SEPARATOR: u16 = 0x003A;

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct PathBuf {
    inner: Vec<u16>,
}

impl PathBuf {
    pub fn new() -> Self {
        Self {
            inner: vec![NUL_TERMINATOR],
        }
    }

    pub fn from_str(s: &str) -> Result<Self, ContainsNul<u16>> {
        let s = U16CString::from_str(s)?;
        Ok(Self::from(s))
    }

    pub fn from_os_str<S: AsRef<OsStr>>(s: S) -> Self {
        let s = U16CString::from_os_str(s).unwrap();
        Self::from(s)
    }

    pub fn push<P: AsRef<Path>>(&mut self, path: P) {
        let path = path.as_ref();
        self.inner.pop(); // Remove NUL
        if let Some(&last) = self.inner.last() {
            if last != MAIN_SEPARATOR && !path.has_root() {
                self.inner.push(MAIN_SEPARATOR);
            }
        }
        self.inner.extend_from_slice(path.as_slice());
        self.inner.push(NUL_TERMINATOR);
    }

    pub fn pop(&mut self) -> bool {
        self.inner.pop(); // Remove NUL
        let Some(end) = self.inner.iter().rposition(|&c| c == MAIN_SEPARATOR) else {
            return false;
        };
        self.inner.truncate(end);
        self.inner.push(NUL_TERMINATOR);
        true
    }

    pub fn as_path(&self) -> &Path {
        debug_assert!(self.inner.last() == Some(&NUL_TERMINATOR));
        unsafe { Path::from_slice_unchecked(&self.inner) }
    }
}

impl ops::Deref for PathBuf {
    type Target = Path;

    fn deref(&self) -> &Self::Target {
        self.as_path()
    }
}

impl AsRef<Path> for PathBuf {
    fn as_ref(&self) -> &Path {
        self.as_path()
    }
}

impl TryFrom<Vec<u16>> for PathBuf {
    type Error = ContainsNul<u16>;

    fn try_from(data: Vec<u16>) -> Result<Self, Self::Error> {
        let s = U16CString::from_vec(data)?;
        Ok(Self::from(s))
    }
}

impl From<U16CString> for PathBuf {
    fn from(s: U16CString) -> Self {
        Self {
            inner: s.into_vec_with_nul(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Path {
    inner: [u16],
}

impl Path {
    pub fn new<S: AsRef<U16CStr> + ?Sized>(s: &S) -> &Self {
        unsafe { Path::from_slice_unchecked(s.as_ref().as_slice_with_nul()) }
    }

    pub fn to_ucstr(&self) -> &U16CStr {
        unsafe { U16CStr::from_slice_unchecked(&self.inner) }
    }

    pub fn from_slice(s: &[u16]) -> Result<&Self, NulError<u16>> {
        let inner = U16CStr::from_slice(s)?;
        Ok(Path::new(inner))
    }

    pub unsafe fn from_slice_unchecked(s: &[u16]) -> &Self {
        // SAFETY: Path is just a wrapper around [u16].
        unsafe { &*(s as *const [u16] as *const Path) }
    }

    pub fn as_slice(&self) -> &[u16] {
        debug_assert_eq!(self.inner.last(), Some(&NUL_TERMINATOR));
        &self.inner[..self.inner.len() - 1]
    }

    pub fn as_slice_with_nul(&self) -> &[u16] {
        &self.inner
    }

    pub fn as_bytes(&self) -> &[u8] {
        bytemuck::must_cast_slice(self.as_slice())
    }

    pub fn as_bytes_with_nul(&self) -> &[u8] {
        bytemuck::must_cast_slice(self.as_slice_with_nul())
    }

    pub fn as_ptr(&self) -> *const u16 {
        self.inner.as_ptr()
    }

    pub fn display(&self) -> Display<'_, U16CStr> {
        self.to_ucstr().display()
    }

    fn attributes(&self) -> u32 {
        // SAFETY: Win32 foreign function.
        unsafe { GetFileAttributesW(self.as_ptr()) }
    }

    pub fn exists(&self) -> bool {
        self.attributes() != INVALID_FILE_ATTRIBUTES
    }

    pub fn is_dir(&self) -> bool {
        let r = self.attributes();
        r != INVALID_FILE_ATTRIBUTES && r & FILE_ATTRIBUTE_DIRECTORY != 0
    }

    pub fn starts_with(&self, path: impl AsRef<Path>) -> bool {
        self.as_slice().starts_with(path.as_ref().as_slice())
    }

    pub fn strip_prefix(&self, base: &Path) -> Result<&Path, StripPrefixError> {
        if self.starts_with(base) {
            let s = &self.as_slice_with_nul()[base.as_slice().len()..];
            Ok(Path::from_slice(s).unwrap())
        } else {
            Err(StripPrefixError(()))
        }
    }

    pub fn join(&self, path: impl AsRef<Path>) -> PathBuf {
        let mut buf = self.to_path_buf();
        buf.push(path);
        buf
    }

    pub fn parent(&self) -> Option<PathBuf> {
        // Get slice without trailing separator
        let slice = self
            .as_slice()
            .strip_suffix(&[MAIN_SEPARATOR])
            .unwrap_or(self.as_slice());
        let Some(end) = slice.iter().rposition(|&c| c == MAIN_SEPARATOR) else {
            return None;
        };
        let mut path = self.to_path_buf();
        path.pop(); // Remove NUL
        path.inner.truncate(end);
        path.inner.push(NUL_TERMINATOR);
        Some(path)
    }

    fn drive(&self) -> Option<u16> {
        if let &[drive, DRIVE_SEPARATOR, ..] = self.as_slice() {
            Some(drive)
        } else {
            None
        }
    }

    pub fn has_root(&self) -> bool {
        if let &[MAIN_SEPARATOR, ..] = self.as_slice() {
            true
        } else {
            self.drive().is_some()
        }
    }

    pub fn is_absolute(&self) -> bool {
        if let &[_, DRIVE_SEPARATOR, MAIN_SEPARATOR, ..] = self.as_slice() {
            true
        } else {
            false
        }
    }

    pub fn is_relative(&self) -> bool {
        !self.is_absolute()
    }

    pub fn to_path_buf(&self) -> PathBuf {
        PathBuf::from(self.to_ucstr().to_owned())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StripPrefixError(());

impl AsRef<Path> for Path {
    fn as_ref(&self) -> &Path {
        self
    }
}

impl AsRef<Path> for U16CStr {
    fn as_ref(&self) -> &Path {
        Path::new(self)
    }
}
