//! A global (cross-process) named mutex.
//!
//! This can be useful to ensure only one instance of a program is running at once.
//!
//! # Example
//! ```no_run
//! use global_mutex;
//!
//! match GlobalMutex::lock("my-mutex") {
//!     Ok(_mutex) => println!("Mutex aquired!");
//!     Err(error) => eprintln!("Failed to aquire mutex: {}", error);
//! }
//! ```

use std::{io, ptr};
use windows_sys::Win32::Foundation::{CloseHandle, GetLastError, ERROR_ALREADY_EXISTS, ERROR_INVALID_HANDLE, HANDLE};
use windows_sys::Win32::System::Threading::CreateMutexA;

/// A global (cross-process) mutex identified by its unique name.
#[derive(Debug)]
pub struct GlobalMutex {
    handle: HANDLE,
}

impl GlobalMutex {
    /// Creates a new global mutex.
    ///
    /// # Arguments
    ///
    /// * `name`: The unique name of the mutex.
    ///
    /// # Returns
    ///
    /// * `Ok(mutex)` if the mutex is successfully created.
    /// * `Err(error)` if a mutex with the same name already exists or if an error occured.
    pub fn lock(name: &str) -> io::Result<Self> {
        // https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-createmutexa
        // SAFETY: Win32 foreign function.
        let handle = unsafe { CreateMutexA(ptr::null_mut(), 0, name.as_ptr()) };
        // SAFETY: Win32 foreign function.
        let error = unsafe { GetLastError() };

        if handle == ERROR_INVALID_HANDLE as _ {
            Err(io::Error::last_os_error())
        } else if error == ERROR_ALREADY_EXISTS {
            // SAFETY: Win32 foreign function.
            unsafe { CloseHandle(handle) };
            Err(io::Error::from(io::ErrorKind::AlreadyExists))
        } else {
            Ok(Self { handle })
        }
    }
}

impl Drop for GlobalMutex {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe { CloseHandle(self.handle) };
    }
}
