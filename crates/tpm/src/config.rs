use serde::{Deserialize, Serialize};
use std::{fs::File, path::Path};

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    pub user: User,
    pub hardware: Hardware,
    /// The packages installation directory.
    pub install_path: Box<Path>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct User {
    pub nickname: Box<str>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Hardware {
    pub screen: Screen,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Screen {
    pub width: u32,
    pub height: u32,
    pub refresh_rate: u32,
}

impl Config {
    pub fn save(&self, path: &Path) -> serde_yaml::Result<()> {
        let file = File::create(path).unwrap();
        serde_yaml::to_writer(file, self)
    }

    pub fn try_load(path: &Path) -> serde_yaml::Result<Option<Self>> {
        if let Ok(file) = File::open(path) {
            serde_yaml::from_reader(file).map(Some)
        } else {
            Ok(None)
        }
    }
}
