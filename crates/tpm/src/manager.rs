//! # The package manager

use crate::{
    client::Client,
    config::Config,
    inventory::{Inventory, State},
    manifest::Manifest,
    script::Script,
    template::Template,
};
use log::{debug, error, trace, warn};
use std::{
    collections::{hash_map::Entry, HashMap},
    fmt::Write,
    fs,
    fs::File,
    io,
    ops::Deref,
    path::{PathBuf, MAIN_SEPARATOR_STR},
    str::FromStr,
};
use tar::Archive;
use uuid::Uuid;
use zstd::Decoder;

/// Package manager
#[derive(Debug)]
pub struct Manager {
    client: Client,
    config: Config,
    inventory: Inventory,
    scripts: HashMap<Uuid, Script>,
}

impl Manager {
    pub fn new(client: Client, config: Config, inventory: Inventory) -> Self {
        Self {
            client,
            config,
            inventory,
            scripts: HashMap::new(),
        }
    }

    pub fn inventory(&self) -> &Inventory {
        &self.inventory
    }

    pub fn path_to_directory(&self, uuid: Uuid) -> PathBuf {
        let package = self.inventory.package(uuid);
        let mut path = self.config.install_path.to_path_buf();
        let s = path.as_mut_os_string();
        let _ = s.write_str(MAIN_SEPARATOR_STR);
        let _ = write!(s, "{}_{}", package.uuid, package.version);
        path
    }

    pub fn path_to_archive(&self, uuid: Uuid) -> PathBuf {
        let package = self.inventory.package(uuid);
        let mut path = self.config.install_path.to_path_buf();
        let s = path.as_mut_os_string();
        let _ = s.write_str(MAIN_SEPARATOR_STR);
        let _ = write!(s, "{}_{}.tar.zst", package.uuid, package.version);
        path
    }

    pub fn path_to_script(&self, uuid: Uuid) -> PathBuf {
        let package = self.inventory.package(uuid);
        let mut path = self.config.install_path.to_path_buf();
        let s = path.as_mut_os_string();
        let _ = s.write_str(MAIN_SEPARATOR_STR);
        let _ = write!(s, "{}_{}.yml", package.uuid, package.version);
        path
    }

    fn url_archive(&self, uuid: Uuid) -> String {
        let package = self.inventory.package(uuid);
        format!("packages/{}_{}.tar.zst", package.uuid, package.version)
    }

    fn url_checksum(&self, uuid: Uuid) -> String {
        let package = self.inventory.package(uuid);
        format!("packages/{}_{}.tar.zst.b3s", package.uuid, package.version)
    }

    fn url_script(&self, uuid: Uuid) -> String {
        let package = self.inventory.package(uuid);
        format!("packages/{}_{}.yml", package.uuid, package.version)
    }

    pub fn update_inventory(&mut self) -> io::Result<()> {
        let manifest = {
            let data = self.client.read_to_string("manifest.yml")?;
            Manifest::from_str(&data).map_err(|e| io::Error::new(io::ErrorKind::Other, e))?
        };
        debug!("{:?}", manifest);
        self.inventory.update(&manifest);
        Ok(())
    }

    pub fn download_package<F>(&mut self, uuid: Uuid, callback: F) -> io::Result<bool>
    where
        F: FnMut(f64, u64) -> bool,
    {
        if self.inventory.state(uuid).contains(State::Installed) {
            trace!("Must uninstall package first");
            return Ok(false);
        }

        let path = self.path_to_archive(uuid);
        let url = self.url_archive(uuid);

        let hash = if let Some(hash) = self.client.download(&url, &path, callback)? {
            hash
        } else {
            debug!("download was cancelled");
            return Ok(false);
        };

        // Fetch checksum file (no progress callback)
        let url = self.url_checksum(uuid);
        let data = self.client.read_to_string(&url)?;

        if !data.is_ascii() {
            error!("invalid checksum file: must be ASCII bytes");
            return Err(io::ErrorKind::InvalidData.into());
        }

        let (file_hash, file_name) = data.split_at(64);
        debug!("{:X?}", file_hash);

        // Verify checksum file starts with 64 hexadecimal charaters
        if !file_hash.chars().all(|c| c.is_ascii_hexdigit()) {
            error!("invalid checksum file: must start 64 hexadecimal charaters");
            return Err(io::ErrorKind::InvalidData.into());
        }

        // Fail if computed checksum is different
        if hash.to_hex().as_str().chars().ne(file_hash.chars()) {
            error!("checksums mistmatch");
            return Err(io::ErrorKind::InvalidData.into());
        }

        let file_name = file_name.trim();
        debug!("{}", file_name);

        // Verify checksum file has correct package file name
        if !path.ends_with(file_name) {
            error!("invalid checksum file: must end with file name");
            debug!("{} - {}", path.to_string_lossy(), file_name);
            return Err(io::ErrorKind::InvalidData.into());
        }

        // Fetch script file
        let url = self.url_script(uuid);
        let data = self.client.read_to_string(&url)?;

        let template = Template::new(|key| match key {
            "BASE_DIRECTORY" => Some(self.path_to_directory(uuid).display().to_string()),
            "SCREEN_WIDTH" => Some(self.config.hardware.screen.width.to_string()),
            "SCREEN_HEIGHT" => Some(self.config.hardware.screen.height.to_string()),
            "SCREEN_REFRESH_RATE" => Some(self.config.hardware.screen.refresh_rate.to_string()),
            "NICKNAME" => Some(self.config.user.nickname.deref().into()),
            _ => {
                warn!("Missing key {key}");
                None
            }
        });

        let data = template
            .apply(&data)
            .ok_or(io::Error::from(io::ErrorKind::InvalidData))?;
        let path = self.path_to_script(uuid);
        fs::write(path, data)?;

        // Mark the package as "downloaded" in the inventory
        self.inventory.state_mut(uuid).insert(State::Downloaded);
        self.inventory.save().unwrap();

        Ok(true)
    }

    pub fn delete_package(&mut self, uuid: Uuid) -> io::Result<bool> {
        if !self.inventory.state(uuid).contains(State::Downloaded) {
            trace!("Must download package first");
            return Ok(false);
        }

        let path = self.path_to_archive(uuid);

        fs::remove_file(path)?;

        self.inventory.state_mut(uuid).remove(State::Downloaded);

        Ok(true)
    }

    pub fn install_package(&mut self, uuid: Uuid) -> io::Result<()> {
        if !self.inventory.state(uuid).contains(State::Downloaded) {
            trace!("Must download package first");
            return Ok(());
        }

        let path = self.path_to_archive(uuid);

        let file = File::open(path)?;
        let decoder = Decoder::new(file)?;
        let mut archive = Archive::new(decoder);

        let path = self.path_to_directory(uuid);

        fs::create_dir(&path)?;
        archive.unpack(path)?;

        self.get_script(uuid)?.install()?;

        self.inventory.state_mut(uuid).insert(State::Installed);
        Ok(())
    }

    pub fn uninstall_package(&mut self, uuid: Uuid) -> io::Result<()> {
        if !self.inventory.state(uuid).contains(State::Installed) {
            trace!("Must install package first");
            return Ok(());
        }

        let path = self.path_to_directory(uuid);
        fs::remove_dir_all(path)?;

        self.get_script(uuid)?.uninstall()?;

        self.inventory.state_mut(uuid).remove(State::Installed);
        Ok(())
    }

    pub fn execute_package(&mut self, uuid: Uuid) -> io::Result<bool> {
        if !self.inventory.state(uuid).contains(State::Installed) {
            trace!("Must install package first");
            return Err(io::ErrorKind::NotFound.into());
        }

        let path = self.path_to_directory(uuid);
        self.get_script(uuid)?.execute(&path)
    }

    fn get_script(&mut self, uuid: Uuid) -> io::Result<&Script> {
        let path = self.path_to_script(uuid);
        match self.scripts.entry(uuid) {
            Entry::Occupied(entry) => Ok(entry.into_mut()),
            Entry::Vacant(entry) => {
                let data = fs::read_to_string(path)?;
                match data.parse() {
                    Ok(script) => Ok(entry.insert(script)),
                    Err(error) => {
                        error!("Failed to parse script: {}", error);
                        Err(io::ErrorKind::InvalidData.into())
                    }
                }
            }
        }
    }
}
