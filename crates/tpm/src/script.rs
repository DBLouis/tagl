use log::trace;
use serde::Deserialize;
use std::{ffi::OsStr, fs, io, ops::Deref, path::Path, process::Command, str::FromStr};

#[derive(Debug, Deserialize, Clone)]
pub struct Script {
    install: Install,
    uninstall: Uninstall,
    execute: Execute,
}

impl Script {
    pub fn install(&self) -> io::Result<()> {
        for step in self.install.steps.iter() {
            step.do_()?;
        }
        Ok(())
    }

    pub fn uninstall(&self) -> io::Result<()> {
        for step in self.uninstall.steps.iter() {
            step.do_()?;
        }
        Ok(())
    }

    pub fn execute(&self, path: &Path) -> io::Result<bool> {
        let mut command = Command::new(&*self.execute.path);
        for arg in self.execute.args.iter() {
            command.arg(OsStr::new(arg.deref()));
        }
        command.current_dir(path);
        trace!("{:?}", command);
        let mut child = command.spawn()?;
        child.wait().map(|status| status.success())
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(transparent)]
struct Install {
    steps: Box<[Step]>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(transparent)]
struct Uninstall {
    steps: Box<[Step]>,
}

#[derive(Debug, Deserialize, Clone)]
enum Step {
    DirectoryCreate { path: Box<Path> },
    FileCreate { path: Box<Path>, content: Box<str> },
}

impl Step {
    fn do_(&self) -> io::Result<()> {
        match self {
            Step::DirectoryCreate { path } => {
                fs::create_dir_all(path)?;
            }
            Step::FileCreate { path, content } => {
                fs::write(path, content.as_bytes())?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, Deserialize, Clone)]
struct Execute {
    path: Box<Path>,
    #[serde(rename = "arguments")]
    args: Box<[Box<str>]>,
}

#[derive(Debug, Deserialize, Clone)]
enum Value {
    Sz(Box<str>),
    DWord(u32),
    QWord(u64),
}

impl FromStr for Script {
    type Err = serde_yaml::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_yaml::from_str(s)
    }
}
