use log::warn;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use std::{fmt, num::ParseIntError, str::FromStr};

#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
    pub revision: u8,
}

impl Version {
    pub fn new(major: u8, minor: u8, patch: u8, revision: u8) -> Self {
        Self {
            major,
            minor,
            patch,
            revision,
        }
    }
}

impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}.{}.{}-{}", self.major, self.minor, self.patch, self.revision)
    }
}

#[derive(Debug)]
pub enum Error {
    Format,
    Parse(ParseIntError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Format => write!(f, "invalid format"),
            Error::Parse(e) => write!(f, "{}", e),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Parse(e) => Some(e),
            _ => None,
        }
    }
}

impl FromStr for Version {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use nom::{
            character::complete::{char, digit1},
            combinator::map_res,
            sequence::tuple,
            IResult,
        };

        fn parse_version(input: &'_ str) -> IResult<&'_ str, Version> {
            let (input, (major, _, minor, _, patch, _, revision)) = tuple((
                map_res(digit1, |s: &str| s.parse::<u8>()),
                char('.'),
                map_res(digit1, |s: &str| s.parse::<u8>()),
                char('.'),
                map_res(digit1, |s: &str| s.parse::<u8>()),
                char('-'),
                map_res(digit1, |s: &str| s.parse::<u8>()),
            ))(input)?;

            Ok((
                input,
                Version {
                    major,
                    minor,
                    patch,
                    revision,
                },
            ))
        }

        let (_, version) = parse_version(s).map_err(|e| {
            warn!("{}", e);
            Error::Format
        })?;
        Ok(version)
    }
}

struct Visitor;

impl<'de> de::Visitor<'de> for Visitor {
    type Value = Version;

    fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str("major.minor.patch-revision")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        value.parse().map_err(de::Error::custom)
    }
}

impl<'de> Deserialize<'de> for Version {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(Visitor)
    }
}

impl Serialize for Version {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.collect_str(self)
    }
}

#[cfg(test)]
mod tests {
    use super::Version;

    #[test]
    fn parse_version() {
        let version: Version = "1.2.3-4".parse().unwrap();
        assert_eq!(version, Version::new(1, 2, 3, 4));
    }
}
