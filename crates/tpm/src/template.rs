use log::trace;
use regex::Regex;
use std::sync::OnceLock;

static R: OnceLock<Regex> = OnceLock::new();

#[derive(Debug)]
pub struct Template<C> {
    context: C,
}

impl<C> Template<C>
where
    C: Fn(&str) -> Option<String>,
{
    pub fn new(context: C) -> Self {
        Self { context }
    }

    pub fn apply(&self, input: &str) -> Option<String> {
        let regex = R.get_or_init(|| Regex::new(r"\$\{[A-Z][A-Z_]*\}").expect("valid regex"));

        let mut output = String::with_capacity(input.len() + input.len() / 2);

        let mut offset = 0;
        for m in regex.find_iter(input) {
            let key = m.as_str().trim_start_matches("${").trim_end_matches('}');
            let value = (self.context)(key)?;
            output.push_str(&input[offset..m.start()]);
            offset = m.end();
            output.push_str(&value);
            trace!("Replacing key '{}' with value '{}'", &key, &value);
        }
        output.push_str(&input[offset..]);

        Some(output)
    }
}
