FROM ubuntu:noble

ENV RUST_VERSION=1.81.0
ENV CARGO_XWIN_VERSION=v0.17.3
ENV CARGO_DENY_VERSION=0.16.1

ENV DEBIAN_FRONTEND=noninteractive
ENV RUSTUP_HOME=/opt/rust/rustup
ENV CARGO_HOME=/opt/rust/cargo
ENV PATH=/home/rust/.cargo/bin:/opt/rust/cargo/bin:/usr/local/musl/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN \
apt-get update && \
apt-get install --yes --quiet --no-install-recommends \
ca-certificates \
clang \
clang-tools \
cmake \
curl \
file \
g++ \
gcc \
git \
libc-dev \
libcurl4-openssl-dev \
libssl-dev \
libxft-dev \
libzstd-dev \
linux-libc-dev \
llvm-18 \
ninja-build \
pkgconf \
sudo \
unzip \
wine \
xutils-dev \
&& \
apt-get clean && \
rm -rf /var/lib/apt/lists/* && \
useradd rust --user-group --create-home --shell /bin/bash --groups sudo && \
curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain "$RUST_VERSION" --profile minimal --no-modify-path --target x86_64-pc-windows-msvc --component rustfmt,clippy,llvm-tools-preview && \
chown -R rust /opt/rust && \
mkdir -p /usr/local/bin && \
ln -s /usr/bin/llvm-rc-18 /usr/local/bin/rc.exe && \
ln -s /usr/bin/llvm-windres-18 /usr/local/bin/windres && \
ln -s /usr/bin/llvm-ar-18 /usr/local/bin/ar

ADD docker/sudoers /etc/sudoers.d/nopasswd

USER rust
RUN \
mkdir -p /home/rust/libs /home/rust/src /home/rust/.cargo/bin && \
ln -s /opt/rust/cargo/config /home/rust/.cargo/config && \
curl -sL https://github.com/rust-cross/cargo-xwin/releases/download/$CARGO_XWIN_VERSION/cargo-xwin-$CARGO_XWIN_VERSION.x86_64-unknown-linux-musl.tar.gz | tar -C /home/rust/.cargo/bin -xz && \
curl -sL https://github.com/EmbarkStudios/cargo-deny/releases/download/$CARGO_DENY_VERSION/cargo-deny-$CARGO_DENY_VERSION-x86_64-unknown-linux-musl.tar.gz | tar -C /home/rust/.cargo/bin --strip-components=1 -xz

WORKDIR /home/rust/src
